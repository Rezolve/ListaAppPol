export class Lista {

	id: number;
	fullname: string;
	b_date: string;
	a_num: number;
	kierunek: string;
	grupa: string;
	kraj: string;
	ulica: string;
	nr_domu: string;
	nr_mieszkania: string;
	kod_pocztowy: string;
	nr_telefonu: string;
	email: string;
	photo: string;
	stud_info: string;

}