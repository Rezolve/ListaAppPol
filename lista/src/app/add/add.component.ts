import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Params, Router } from '@angular/router';

import { Lista } from '../lista';

declare var $: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

	public file_src: string = '../assets/images/noavatar.png';

  constructor(private router:Router) { }

  ngOnInit() {
  }
	model = new Lista();

	imageUploaded(file: any) {
  		$('img').hide();
  	}

  	imageRemoved (file: any) {
  		$('img').show();
  	}

  	goBack() {
  		this.router.navigate(['/home']);
  	}

}
