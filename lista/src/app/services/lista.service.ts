import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ListaService {

	//Laravel local server
	server = 'http://lista/';

	headers: Headers = new Headers; 
	options: any;

  constructor(private http:Http) {
  	this.headers.append('enctype', 'multipart/form-data');
  	this.headers.append('Content-Type', 'application/json');
  	this.headers.append('X-Requested-With', 'XMLHttpRequest');
  	this.options = new RequestOptions({ headers: this.headers });
  }

}
